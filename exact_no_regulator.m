%
%This script will generate the classical and quantum values of: <x(t)>,
%<p(t)>,<x^2(t)>, and <p^2(t)> of a single particle starting in a coherent
%state, alpha, quenched into the anharmonic Hamiltonian: 
%   H = hbar*w*(n_operator + 1/2) + lambda * x^4
%
%This script will also plot these values as a function of time
%
%However, fiddling with the get/plot parameters at the beginning of the
%script will allow you to optionally exclude some calculations or plots.
%
%Note: alpha, lambda, timesteps, and t_final should be defined outside

%clear all;

%Notes regarding our initial coherent state:
%   - |alpha| << cutoff
%   - semi-classical when |alpha| >> 1

%{
%initial state
alpha = 0 + 1i;
lambda = 10;
%time interval
timesteps = 100;
%}

%%%%% SETUP PARAMETERS %%%%%
%get/plot parameters
getclassical = 0;
getx2 = 0;
getp2 = 0;
plotjustx = 1;
dontplotanything = 0;
%cutoff = highest n used for matrix calculation
cutoff = 1000;
%constants
w = 1;
hbar = 1;
mass = 1;
%times to make calculations
tf = t_final;
timespace = linspace(0,tf,timesteps);

%%%%% SETUP INITIAL STATE %%%%%
st_alpha = exp(-(abs(alpha))/2)*ones(cutoff+1,1);
for j = 1:cutoff
    st_alpha(j+1:end) = st_alpha(j+1:end) * alpha/sqrt(j);
end
%initial state normalization
norm = 1/sqrt(ctranspose(st_alpha)*st_alpha);
st_alpha = norm*st_alpha;

%%%%% SETUP OPERATORS %%%%%
X = X_creator(cutoff, w, hbar, mass);
P = P_creator(cutoff, w, hbar, mass);
H = H_creator(cutoff, w, hbar, mass, lambda);
[evec, eval] = eig(H);

%%%%% CALCULATE TIME DEPENDENCE %%%%%
%messages to let everyone know things are still working
disp('beginning calculations');
%holds evolved w.f.
st_alpha_time = st_alpha;
%index variable
counter = 0;
%make vectors to hold expectation values
x_t = zeros(size(timespace));
p_t = zeros(size(timespace));
if getx2
    X2 = X*X;
    x2_t = zeros(size(timespace));
end
if getp2
    P2 = P*P;
    p2_t = zeros(size(timespace));
end
%loop over all calculation times
for t = timespace
    %update index
    counter = counter+1;
    
    %add dependence
    %Note the use of "diag(diag( exp_time_factor )".
    %This is because exp(diagonal matrix) will give the values we want for
    %the respective vector elements on the diagonal, but 1.0s everywhere 
    %else. So diag(diag(...)) turns those 1s into 0s and keeps the diagonal
    st_alpha_time = evec * (diag(diag(exp(-1i*t*eval/hbar))) * transpose(evec) * st_alpha);
    
    %calculate and store observables
    x_t(counter) = ctranspose(st_alpha_time) * X * st_alpha_time;
    p_t(counter) = ctranspose(st_alpha_time) * P * st_alpha_time;
    if getx2
        x2_t(counter) = ctranspose(st_alpha_time) * X2 * st_alpha_time;
    end
    if getp2
        p2_t(counter) = ctranspose(st_alpha_time) * P2 * st_alpha_time;
    end
end
disp('Done!');

%------------------------- Plot -------------------------
%Note: matlab has some vestigial imaginary terms in the form of:
%x_t = real(x_t) + 10^-30 * 1i
%so the only the real values are graphed.
if getclassical
    [t,y] = classical_result(w, lambda, mass, alpha, tf, hbar);
end

if dontplotanything
    %just don't plot
elseif plotjustx
    figure('units','normalized','outerposition',[0 0 1 1])
    title(['alpha: ', num2str(alpha),', cutoff: ', num2str(cutoff), ', lambda: ',num2str(lambda)]);
    xlabel('time, t');
    ylabel('expectation values');
    hold on;
    
    plot(timespace, real(x_t), '-');
    grid on;
    
    legend('x(t)');
else
    figure('units','normalized','outerposition',[0 0 1 1])
    title(['alpha: ', num2str(alpha),', cutoff: ', num2str(cutoff), ', lambda: ',num2str(lambda)]);
    xlabel('time, t');
    ylabel('expectation values');
    hold on;
    
    plot(timespace, real(x_t), '-');
    hold on;
    plot(timespace, real(p_t), '-');
    if getx2
        hold on;
        plot(timespace, real(x2_t), '-');
    end
    if getp2
        hold on;
        plot(timespace, real(p2_t), '-');
    end
    if getclassical
        hold on;
        plot(t, y(:,1), 'o');
        hold on;
        plot(t, y(:,2), 'o');
        if getx2
            hold on;
            plot(t, y(:,1).^2, 'o');
        end
        if getp2
            hold on;
            plot(t, y(:,2).^2, 'o');
        end
    end
    grid on;
    
    if getx2
        if getp2
            legend('x(t) quantum', 'p(t) quantum', 'x^2(t) quantum', 'p^2(t) quantum', 'x(t) classical', 'p(t) classical', 'x^2(t) classical', 'p^2(t) classical');
        else
            legend('x(t) quantum', 'p(t) quantum', 'x^2(t) quantum', 'x(t) classical', 'p(t) classical', 'x^2(t) classical');
        end
    elseif getp2
        legend('x(t) quantum', 'p(t) quantum', 'p^2(t) quantum', 'x(t) classical', 'p(t) classical', 'p^2(t) classical');
    else
        legend('x(t) quantum', 'p(t) quantum', 'x(t) classical', 'p(t) classical');
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------------------- Hamiltonian Operator function -------------------------
%remember: H = hbar*w*(n_operator + 1/2) + lambda * x^4
%or H = (p'^2/2m + x'^2 *mw/2 + e*x'^4) *alpha^2,where ep = lambda/alpha^2
function H = H_creator(cutoff, w, hbar, m, lambda)
    p = P_creator(cutoff, w, hbar, m);
    x = X_creator(cutoff, w, hbar, m);
    H = p*p/(2*m) + x*x*m*w*w/2 + x*x*x*x*lambda;
end

%------------------------- position operator function -------------------------
%remember: X = (a^dag + a)*sqrt(hbar/(2*m*w))
function x = X_creator(cutoff, w, hbar, m)
    x = zeros(cutoff+1,cutoff+1);
    for n = 0:cutoff
        if ((n-1) >= 0)
            x((n+1)-1,(n+1)) = sqrt(n);
        end
        if ((n+1) <= cutoff)
            x((n+1)+1,(n+1)) = sqrt(n+1);
        end
    end
    x = x*sqrt(hbar/(2*m*w));
end

%------------------------- momentum operator function -------------------------
%remember: P = i*(a^dag - a)*sqrt(m*w*hbar/2)
function p = P_creator(cutoff, w, hbar, m)
    p = zeros(cutoff+1,cutoff+1);
    for n = 0:cutoff
        if ((n-1) >= 0)
            p((n+1)-1,(n+1)) = -sqrt(n);
        end
        if ((n+1) <= cutoff)
            p((n+1)+1,(n+1)) = sqrt(n+1);
        end
    end
    p = p*1i*sqrt(m*w*hbar/2);
end

%------------------------- classical result function -------------------------
%remember: H = p^2/2m + mw^2/2*x^2 + lambda*x^4
function [t,y] = classical_result(w, lambda, m, alpha, tf, hbar)
    %initial conditions
    y0 = [real(alpha)*sqrt(2*hbar/(m*w)); imag(alpha)*sqrt(2*hbar/(m*w))];
    
    %system of ODEs
    hamilt_ode_classical = @(t,y) [y(2)/m; -(m*w^2*y(1) + 4*lambda*y(1)^3)];
    
    %solving the ode
    [t,y] = ode45(hamilt_ode_classical, [0, tf], y0);
end
