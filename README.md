# README #

### What is this repository for? ###

* 'exact no regulator' and 'single run MHMC reg keld' are used together to produce a single markov chain monte carlo simulation of our single particle system.
* 'lindblad_krylov' is used to create the exact results we compare our data to by use of krylov time evolution. It solves the same system as 'exact no regulator', but with the regulator term.
* 'multiple_run_MHMC' is used to run 'single run MHMC reg keld' many times so we can average over our uncorrelated chains.
* 'multiple_run_extract' allows us to revisit the data saved from 'multiple_run_MHMC' or 'single run MHMC reg keld'.

* To read more about the system and the regulating term go here: https://www.overleaf.com/read/xhtkzhpzkbtx

### How do I get set up? ###

* To run 'lindblad_krylov', you will need expmv_tspan.m . To get this file and its helper files, go to: https://github.com/higham/expmv


### Who do I talk to? ###

patrick.koch07@gmail.com