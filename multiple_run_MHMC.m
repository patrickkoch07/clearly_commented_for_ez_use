%With this code, once the stdo and num of calcs are set in 
%'single_run_MHMC_reg_keld.m', we can initialize the parameters here to run
%that code multiple times. This is so we can then average over the data and
%obtain error bars for ours results.

%%%%% INITIALIZE PARAMETERS %%%%%
N = 3;
total_runs = 50;
t_final = .5;
eps = 5;
lambda = 10;
alpha = 0 + 1i;
%hold the data
culm_x_n = zeros(N+1, total_runs);
culm_norm = zeros(total_runs,1);
total_change = zeros(total_runs,1);

%%%%% LOOP OVER RUNS %%%%%
for i = 1:total_runs
    run_number = i;
    single_run_MHMC_reg_keld_speed;
    %store some data from current runs
    culm_x_n(:,i) = x_n;
    culm_norm(i) = norm;
    total_change(i) = sum(changes_perc(end-1,:))/numOfCalcs * 100;
end

%%%%% DISPLAY DATA PRE-AVERAGING %%%%%
for i = 1:total_runs
    disp(['Updates accepted: ', num2str(total_change(i)), '% and norm: ', num2str(culm_norm(i)),' in run: ', num2str(i),'/',num2str(total_runs)]);
end
%plot x_n of all runs
figure();
plot(linspace(0,t_final,N+1),culm_x_n);
legend();
%decide to not consider a run due to some craziness
bad_runs = [];
X_N = [];
NORM_N = [];
for i = 1:total_runs
    if isempty(find(bad_runs == i,1))
        X_N = [X_N,culm_x_n(:,i)];
        NORM_N = [NORM_N,culm_norm(i)];
    end
end

%%%%% DISPLAY AVG DATA %%%%%
lindblad_krylov;
hold on;
errorbar(linspace(0,t_final,N+1), sum(transpose(X_N))/length(X_N), std(transpose(X_N)));
title(['Lambda = ', num2str(lambda),', alpha = ', num2str(alpha), ', N = ', num2str(N), ', Calc Steps = ', num2str(numOfCalcs), ', Epsilon = ', num2str(eps), ', Runs= ', num2str(total_runs)]);
hold on;
errorbar(linspace(0,t_final,N+1), sum(transpose(X_N .* NORM_N))/sum(NORM_N), std(transpose(X_N)) .* sqrt(sum((NORM_N/sum(NORM_N)).^2)));
hold on;
legend('Krylov Time evolution', 'Averaged MC', 'Weighted Average MC');
