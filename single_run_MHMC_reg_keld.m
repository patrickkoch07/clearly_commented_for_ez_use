%
%The purpose of this script is to sample over a single particle's path with
%Hamilontian: (n_operator + 1/2) + lambda * x^4 from t = 0 to 't_final',
%with initial condition 'alpha' of the SHO's coherent states.
%
%This script will generate and sample the particle's path using MHMC along a
%fourier or momentum basis. 
%
%In the MHMC update step, the proposed path is
%chosen based on the addition of the current path with some basis times a 
%random number from a gaussian distribution centered at 0 with a std of STD.
%
%STD varies in value from 'stdo*10' to 'stdo/10' based on current path's
%distribution function value and distribution function's derivative along
%that basis direction.
%
%Note: N, run_number, fname1, fname2, fname3 should be defined outside
%Note2: Data is saved to: currentdir/data/lam(lambda)/N(N)_t(t_final)/N(N)_r(run_number)_*.out

%%%%% BEGIN INITIALIZING PARAMETERS %%%%%
clear_files = 1;
store_data = 1;

%{
N = 3;
run_number = 0;
t_final = .5;
eps = 5;
lambda = 10;
alpha = 0 + 1i;
%}
timesteps = N + 1;

%make directory and files for storage
% {
mkdir('data', ['lam',num2str(lambda)]);
mkdir(['data/lam',num2str(lambda)], ['N',num2str(N),'_t',num2str(t_final)]);
fname1 = ['data/lam',num2str(lambda),'/N',num2str(N),'_t',num2str(t_final),'/N',num2str(N),'_r',num2str(run_number),'_y_n.out'];
fname2 = ['data/lam',num2str(lambda),'/N',num2str(N),'_t',num2str(t_final),'/N',num2str(N),'_r',num2str(run_number),'_Fs.out'];
fname3 = ['data/lam',num2str(lambda),'/N',num2str(N),'_t',num2str(t_final),'/N',num2str(N),'_r',num2str(run_number),'_x_n.out'];
fname4 = ['data/lam',num2str(lambda),'/N',num2str(N),'_t',num2str(t_final),'/N',num2str(N),'_r',num2str(run_number),'_dFs.out'];
%}

%optional clearing of fname1-4:
% {
if clear_files
    fout = fopen(fname1, 'w');
    fclose(fout);

    fout = fopen(fname2, 'w');
    fclose(fout);

    fout = fopen(fname3, 'w');
    fclose(fout);

    fout = fopen(fname4, 'w');
    fclose(fout);
end
%}

%%%%% GET 'x_t' TO INITIALIZE 'y_n' %%%%%
%runs exact_no_regulator.m w/ params from this script
exact_no_regulator;

%%%%% FINISH INITIALIZING PARAMETERS %%%%%
dt = t_final/N;
epsilon = eps*dt;
%change the std of our montecarlo update step depending on our dt or (t_final = 1.5) N.
if N == 3
    numOfCalcs = 1*10^6;
    stdo = .15 * [3,1,2,1];
elseif N == 5
    numOfCalcs = 1*10^6;
    stdo = .13 * [.5,1.2,1.2,.5 , .5,.25];
elseif N == 10
    numOfCalcs = 1*10^6;
    stdo = .15 * [.5,1,1,.75 , .25,.2,.1,.08 , .07,.07,.04];  
elseif N == 15
    numOfCalcs = 1*10^5;
    stdo = .08 * [6,10,5,3 , 2,.5,.25,.2 , .125,.125,.085,.07 , .07,.07,.07,.07];
elseif N == 20
    numOfCalcs = 1*10^6;
    stdo = .08*[1,2,2.25,1.5 , .5,.4,.25,.2 , .125,.125,.085,.07 , .0625,.0625,.0625,.0625 , .04,.04,.04,.04, .03125];
elseif N == 25
    numOfCalcs = 1*10^5;
    stdo = 2*.035*[5,6,6,7 , 6,.5,.25,.2 , .125,.125,.085,.07 , .0625,.0625,.0625,.0625 , .04,.04,.04,.04, .03125,.03125,.03125,.03125 , .03125,.03125];
else
    numOfCalcs = 1*10^5;
    stdo = .03*[8,10,6,4 , 1,.6,.3,.3 , .15,.15,.1,.1 , .09,.09,.09,.09 , .08,.08,.08,.08, .07,.07,.07,.05 , .05,.05,.05,.05 , .03,.03,.03];
end
%after this many update steps, export our current data to fname1, fname2
when_to_export = 5*10^6;
y_n = transpose(real(x_t) + 0);
%these two variables will hold the data before we export
if store_data
    capture_y_n = zeros(when_to_export, length(y_n));
    capture_Fs = zeros(when_to_export,1);
    capture_dFs = zeros(when_to_export,length(y_n));
end

%holds the number of times changes were accepted in our update step
changes_perc = zeros(floor(numOfCalcs/length(y_n))+1, length(y_n));

%%%%% CALCULATION STEP 0 %%%%%
[dFs, Fs] = distrib_function(y_n, alpha, dt, epsilon, lambda);

x_n = y_n * Fs;
p_n = ((y_n(2:end) - y_n(1:end-1))/dt) * Fs;
norm = Fs;
if store_data
    capture_y_n(1,:) = transpose(y_n);
    capture_Fs(1) = Fs;
    capture_dFs(1,:) = transpose(dFs);
end

%%%%% CALCULATION LOOP %%%%%
for j = 1:numOfCalcs
    %simple output to update user on loop's progress
    if j == round((numOfCalcs)/5)
        disp('a fifth is done');
    elseif j == round((numOfCalcs)/5) * 2
        disp('two fifths are done');
    elseif j == round((numOfCalcs)/5) * 3
        disp('three fifths are done');
    elseif j == round((numOfCalcs)/5) * 4
        disp('four fifths are done');
    end
    
    %After 'when_to_export' updates, lets export the data to fname1,fname2
    if store_data
        if mod(j,when_to_export) == 0
            dlmwrite(fname1, capture_y_n, '-append', 'delimiter', ' ', 'roffset', 0);
            dlmwrite(fname2, capture_Fs, '-append', 'delimiter', ' ', 'roffset', 0);
            dlmwrite(fname4, capture_dFs, '-append', 'delimiter', ' ', 'roffset', 0);
            %reset for further capture
            capture_y_n = zeros(when_to_export, length(y_n));
            capture_Fs = zeros(when_to_export,1);
            capture_dFs = zeros(when_to_export, length(y_n));
        end
    end
    
    %update y_n
    [dFs,Fs,y_n,change] = update_step(dFs, Fs, y_n, alpha, dt, epsilon, lambda, j, stdo);
    %hold the data
    
    x_n = y_n * Fs + x_n;
    p_n = ((y_n(2:end) - y_n(1:end-1))/dt) * Fs + p_n;
    norm = Fs + norm;
    
    if floor(j/length(y_n)) == 0
        changes_perc(floor(j/length(y_n))+1,mod(j,length(y_n))+1) = change;
    else
        changes_perc(floor(j/length(y_n))+1,mod(j,length(y_n))+1) = change + changes_perc(floor(j/length(y_n)),mod(j,length(y_n))+1);
    end
    if store_data
        capture_y_n(mod(j,when_to_export)+1,:) = transpose(y_n);
        capture_Fs(mod(j,when_to_export)+1) = Fs;
        capture_dFs(mod(j,when_to_export)+1,:) = transpose(dFs);
    end
end
if store_data
    %export the last of the data
    dlmwrite(fname1, capture_y_n(1:mod(j,when_to_export)+1,:), '-append', 'delimiter', ' ', 'roffset', 0);
    dlmwrite(fname2, capture_Fs(1:mod(j,when_to_export)+1), '-append', 'delimiter', ' ', 'roffset', 0);
    dlmwrite(fname4, capture_dFs(1:mod(j,when_to_export)+1,:), '-append', 'delimiter', ' ', 'roffset', 0);
end
%%%%% FINISH CALCULATIONS %%%%%
x_n = x_n/norm;
%output the total found x_n
fout = fopen(fname3, 'w');
fprintf(fout,'%f\n',x_n);
fclose(fout);

%simple output to quickly see how the run went
disp('x_n: ');
disp(transpose(x_n));
disp(['changes: ', num2str(sum(changes_perc(end-1,:))/(numOfCalcs-21) * 100), '%']);
disp(['norm: ', num2str(norm)]);

%%%%% PLOT %%%%%
    %distribution function
%{
figure('units','normalized','outerposition',[0 0 1 1]);
title(['Lambda = ', num2str(lambda),', alpha = ', num2str(alpha), ', N = ', num2str(N), ', Calc Steps = ', num2str(numOfCalcs), ', Epsilon = ', num2str(epsilon/dt), ', Run: ', num2str(run_number)]);

subplot(2,1,1);
plot(0:numOfCalcs, log(abs(capture_Fs(1:mod(j,when_to_export)+1))));
xlabel('MonteCarlo Update Step');
ylabel('LOG(| Distribution Function |)')
subplot(2,1,2);
plot(0:numOfCalcs, sign(capture_Fs(1:mod(j,when_to_export)+1)));
xlabel('MonteCarlo Update Step');
ylabel('Distribution Function sign')
%}
    %distribution function derivative
%{
counter = 0;
for i = 1:length(y_n)
    if counter == 0
        figure('units','normalized','outerposition',[0 0 1 1]);
    end
    counter = counter + 1;
    
    subplot(5,4,counter);
    plot(0:numOfCalcs, capture_dFs(1:mod(j,when_to_export)+1,i)./capture_Fs(1:mod(j,when_to_export)+1));
    title(['Derivative w.r.t y', num2str(i-1)]);
    
    if counter == 20
        counter = 0;
    end
end
%}
    %update acceptance %
%{
counter = 0;
for i = 1:length(y_n)
    if counter == 0
        figure('units','normalized','outerposition',[0 0 1 1]);
    end
    counter = counter + 1;
    
    subplot(5,4,counter);
    plot(1:floor(numOfCalcs/length(y_n))+1, changes_perc(:,i)./transpose((1:floor(numOfCalcs/length(y_n))+1)) * 100);
    title(['Changes in fourier basis: ', num2str(i-1), ', ', num2str(changes_perc(end-1,i)/(floor(numOfCalcs/length(y_n))+1) *100), '% change']);
    
    if counter == 20
        counter = 0;
    end
end
%}
    %<x(t)> from various parts of the chain
%{
figure('units','normalized','outerposition',[0 0 1 1]);
plot(linspace(0,t_final,N+1), transpose(x_n));
xlabel('time, t');
ylabel('Expectation of x')
title(['Lambda = ', num2str(lambda),', alpha = ', num2str(alpha), ', N = ', num2str(N), ', Calc Steps = ', num2str(numOfCalcs), ', Epsilon = ', num2str(epsilon/dt), ', Run: ', num2str(run_number)]);
hold on;
plot(timespace, real(x_t), '-.');
if store_data
    hold on;
    plot(linspace(0,t_final,N+1), sum(capture_Fs(floor(numOfCalcs/2):end) .* capture_y_n(floor(numOfCalcs/2):end,:))/sum(capture_Fs(floor(numOfCalcs/2):end)));
    hold on;
    plot(linspace(0,t_final,N+1), sum(capture_Fs(1:floor(numOfCalcs/10)) .* capture_y_n(1:floor(numOfCalcs/10),:))/sum(capture_Fs(1:floor(numOfCalcs/10))));
end
%hold on;
%plot(time,x_t_lind, '-.');
legend('MC, all', 'exact (no epsilon)', 'MC, first 50% removed', 'MC, only first 10%', 'exact, ep = 10');
%}
    %<x(t)>,<p(t)>
%{
%figure('units','normalized','outerposition',[0 0 1 1]);
hold on;
plot(linspace(0,t_final,N+1), transpose(x_n));
xlabel('time, t');
ylabel('Expectation values')
title(['Lambda = ', num2str(lambda),', alpha = ', num2str(alpha), ', N = ', num2str(N), ', Calc Steps = ', num2str(numOfCalcs), ', Epsilon = ', num2str(epsilon/dt), ', Run: ', num2str(run_number)]);
%hold on;
%plot(timespace, real(x_t), '-.');
hold on;
plot(linspace(0,t_final-dt,N), p_n/norm);

legend('exact, <x(t)>', 'exact, <p(t)>', 'MC, <x(t)>', 'MC, <p(t)>');
%}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% FUNCTIONS %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%% UPDATE STEP FUNCTION %%%%%
function [dFsNew, FsNew, y_f, change] = update_step(dFs, Fs, y_f, alpha, delta, epsilon, lambda, index, stdo)
    %scales distribution/derivative ratio to fit nicely between min and max std limits
    scalefactor = .4;
    %holds which basis to update
    current = mod(index,length(y_f));
    %sets min and max std limits
    facto = 1000;
    min_std = stdo(current+1) / facto;
    max_std = stdo(current+1) * facto;
    %holds the basis
    
    %fourier
    proposal_vec = cos(pi*(0:(length(y_f)-1))/(length(y_f)-1) * current)/sqrt(length(y_f));
    
    %momentum
    %proposal_vec = get_proposal_vec(length(y_f), current);
    
    %get range of current position distribution/deriv
    direc_deriv = sum(dFs .* transpose(proposal_vec));
    range = abs(Fs/direc_deriv) * scalefactor;
    %bound it
    if range > max_std
        range = max_std;
    end
    if range < min_std
        range = min_std;
    end
    
    %use the range to propose new step
    random_jump = real(normrnd(0, range));
    proposal = transpose(proposal_vec) * random_jump;
    %calculate update's range
    [dFsPropose,FsPropose] = distrib_function(y_f + proposal, alpha, delta, epsilon, lambda);
    direc_deriv_prop = sum(dFsPropose .* transpose(proposal_vec));
    propose_range = abs(FsPropose/direc_deriv_prop) * scalefactor;
    %bound it
    
    %ALSO: 
    %   If we had an update's deriv AND func to be 0, we are at a fringe so
    %   just never accept. Imagine our distribution function being approx 
    %   by a normal distribution: at the ends, x-> inf, the function -> 0 
    %   and deriv -> 0 too.
    %to fix we pick a new value
    while isnan(propose_range)
        %use the range to propose new step
        random_jump = real(normrnd(0, range));
        proposal = transpose(proposal_vec) * random_jump;
        %calculate update's range
        [dFsPropose,FsPropose] = distrib_function(y_f + proposal, alpha, delta, epsilon, lambda);
        direc_deriv_prop = sum(dFsPropose .* transpose(proposal_vec));
        propose_range = abs(FsPropose/direc_deriv_prop) * scalefactor;
        %bound it
    end
    if propose_range > max_std
        propose_range = max_std;
    end
    if propose_range < min_std
        propose_range = min_std;
    end
    
    %calc prob of acceptance based on current position
    prob = abs(FsPropose/Fs);
    %additional conditional prob based on different derivs/update stds
    %cond_prob = (normpdf(random_jump, 0, propose_range)/normpdf(0, 0, propose_range)) / (normpdf(random_jump, 0, range)/normpdf(0, 0, range));
    cond_prob = normpdf(random_jump, 0, propose_range) / normpdf(random_jump, 0, range);
    
    %together, find the prob
    prob = min(1, prob * cond_prob);
    
    %accept or not
    random = rand;
    if random <= prob
        y_f = y_f + proposal;
        dFsNew = dFsPropose;
        FsNew = FsPropose;
        change = 1;
    else
        change = 0;
        dFsNew = dFs;
        FsNew = Fs;
    end
end

%%%%% DISTRIBUTION FUNCION %%%%%
function [dFs, Fs] = distrib_function(y_f, alpha, delta, epsilon, lambda)
    %maximum value of the distribution function
    limit = 10^40;
    %maximum value of the derivative components
    deriv_limit = limit;
    %minimum value of the derivative components
    min_limit = 10^-40;
        
    %Fixing issues with y_n = 0 because we will have terms ~ 1/y_n
    %   Upon visual examination, it looks like as y_n -> 0 from the left 
    %   and right, a finite number is approached rather quickly (for our 
    %   high, 10, lambda). So, I'll just approximation y_n = 0 by +/- 10^-4
    for i = 1:length(y_f)
        if abs(y_f(i)) == 0
            y_f(i) = 10^-4;
        end
        if abs(y_f(i)) < 10^-4
            y_f(i) = 10^-4 * sign(y_f(i));
        end
    end
    %}

    %%%%% Distribution Function's Setup %%%%%
    ddoty = derivative_2nd(y_f,delta);
    %A_n
    A_n = (-delta) * (2*ddoty + 2*y_f + 8*lambda*y_f.^3);
    A_n(1) = (-delta) * (2*ddoty(1) + 2*y_f(1) + 8* lambda *y_f(1).^3) + 2*sqrt(2)*imag(alpha) - 2*(y_f(2)-y_f(1))/delta;
    %B_n
    B_n = ones(size(A_n)) * -epsilon;
    B_n(1) = -1 - epsilon;
    %C_n
    C_n = (-delta)*8*lambda*y_f;
    %G_n
    G_n = A_n + B_n.^2 ./ (3*C_n);
    
    %%%%% Distribution Funcion %%%%%
    %
    Fs = ones(length(y_f)-1, 1);
    
    R = G_n./real_1_by_3(3*C_n);
    for j = 1:(length(y_f)-1)
        Fs(j) = 2 * pi * real(airy(0,R(j),1));  
        Fs(j) = Fs(j) * real(exp((-2/3)*(R(j)).^(3/2) - (A_n(j).*B_n(j))./(3*C_n(j)) - (2*B_n(j).^3)./(27*C_n(j).^2)) ./ abs(3*C_n(j)).^(1/3));
    end
    
    Fs = exp(-(y_f(1) - sqrt(2)*real(alpha))^2) * prod(Fs(1:end));
    
    %%%%% CLEANING 'Fs' %%%%%
    %Incase the earlier y_n -> 0 issues weren't completely fixed
    if isnan(Fs)
        Fs = 0;
    end
    %fix to be within limits
    % {
    if Fs == inf
        Fs = limit*sign(Fs);
    end
    if Fs == -inf
        Fs = limit*sign(Fs);
    end
    %}
    if abs(Fs) > limit
        Fs = limit*sign(Fs);
    end
    
    %%%%% Derivative setup %%%%%
    d_C_n = -delta * 8 * lambda * ones(size(y_f));
    d_A_n = -delta * (2*(-2/delta^2) + 2 + 24*lambda*y_f.^2);
    %because of forward difference
    d_A_n(1) = -delta * (2*(1/delta^2) + 2 + 24*lambda*y_f(1)^2) + 2/delta;
    d_G_n = d_A_n - (B_n.^2 ./ (3*C_n.^2)).*d_C_n;
    
    L_n = ((A_n.*B_n)./(3*C_n.^2) + 4*B_n.^3 ./(27*C_n.^3) - 1./(3*C_n)) .* d_C_n;
    M_n_0 = -B_n./(3*C_n) .* d_A_n;
    K_n_0 = 1./real_1_by_3(3*C_n) .* d_G_n + G_n./(-3*C_n.*real_1_by_3(3*C_n)) .* d_C_n;
    
    d_A_n_m1 = -2*delta*(1/delta^2) * ones(size(A_n));
    %because of forward difference, & A(1) has a normal derivative term
    d_A_n_m1(1) = -2*delta*(-2/delta^2) - 2/delta;
    d_G_n_m1 = d_A_n_m1;
    %recall that M_0_m1 and K_0_m1 aren't used
    M_n_m1 = [0;B_n]./(3 * [0;C_n]) .* [0;d_A_n_m1];
    M_n_m1 = -M_n_m1(1:end-1);
    K_n_m1 = 1 ./ real_1_by_3([0;3*C_n]) .* [0;d_G_n_m1];
    K_n_m1 = K_n_m1(1:end-1);
       
    d_A_n_m1 = -2*delta*(1/delta^2) * ones(size(A_n));
    M_2_m2 = [0;0;B_n]./(3 * [0;0;C_n]) .* [0;0;d_A_n_m1];
    M_2_m2 = -M_2_m2(3);
    K_2_m2 = 1 ./ real_1_by_3([0;0;3*C_n]) .* [0;0;d_A_n_m1];
    K_2_m2 = K_2_m2(3);

    d_A_n_p1 = -2*delta*(1/delta^2) * ones(size(A_n));
    d_G_n_p1 = d_A_n_p1;
    %M_N-1_p1 and K_N-1_p1 aren't used
    M_n_p1 = [B_n;0]./(3 * [C_n;0]) .* [d_A_n_p1;0];
    M_n_p1 = -M_n_p1(2:end);
    K_n_p1 = 1 ./ real_1_by_3([3*C_n;0]) .* [d_G_n_p1;0];
    K_n_p1 = K_n_p1(2:end);
    
    %%%%% Derivative Function %%%%%
    dFs = zeros(size(y_f));
    for j = 1:length(y_f)
        %y_0
        if j == 1
            dFs(j) = -2*(y_f(1) - sqrt(2)*real(alpha)) + L_n(j) + M_n_0(j);
            dFs(j) = dFs(j) + K_n_0(j) * airy(1,R(j),1)/airy(0,R(j),1);
            
            dFs(j) = dFs(j) + M_n_p1(j);
            dFs(j) = dFs(j) + K_n_p1(j) * airy(1,R(j+1),1)/airy(0,R(j+1),1);
        %y_2
        elseif j == 3
            dFs(j) = M_2_m2;
            dFs(j) = dFs(j) + K_2_m2 * airy(1,R(j-2),1)/airy(0,R(j-2),1);
            
            dFs(j) = dFs(j) + M_n_m1(j);
            dFs(j) = dFs(j) + K_n_m1(j) * airy(1,R(j-1),1)/airy(0,R(j-1),1);
            
            if j ~= length(y_f)
                dFs(j) = dFs(j) + L_n(j) + M_n_0(j);
                dFs(j) = dFs(j) + K_n_0(j) * airy(1,R(j),1)/airy(0,R(j),1);
                
                if j ~= length(y_f)-1
                    dFs(j) = dFs(j) + M_n_p1(j);
                    dFs(j) = dFs(j) + K_n_p1(j) * airy(1,R(j+1),1)/airy(0,R(j+1),1);
                end
            end
        %y_N-1
        elseif j == length(y_f)-1
            dFs(j) = M_n_m1(j);
            dFs(j) = dFs(j) + K_n_m1(j) * airy(1,R(j-1),1)/airy(0,R(j-1),1);
            
            dFs(j) = dFs(j) + L_n(j) + M_n_0(j);
            dFs(j) = dFs(j) + K_n_0(j) * airy(1,R(j),1)/airy(0,R(j),1);
        %y_N
        elseif j == length(y_f)
            dFs(j) = M_n_m1(j);
            dFs(j) = dFs(j) + K_n_m1(j) * airy(1,R(j-1),1)/airy(0,R(j-1),1);
        %y_n
        else
            dFs(j) = M_n_m1(j);
            dFs(j) = dFs(j) + K_n_m1(j) * airy(1,R(j-1),1)/airy(0,R(j-1),1);
            
            dFs(j) = dFs(j) + L_n(j) + M_n_0(j);
            dFs(j) = dFs(j) + K_n_0(j) * airy(1,R(j),1)/airy(0,R(j),1);
            
            dFs(j) = dFs(j) + M_n_p1(j);
            dFs(j) = dFs(j) + K_n_p1(j) * airy(1,R(j+1),1)/airy(0,R(j+1),1);
        end
        
        %%%%% CLEANING 'dFs(j)' %%%%%
        dFs(j) = real(dFs(j)) * Fs;
        
        if isnan(dFs(j))
            dFs(j) = 0;
        end
        if abs(dFs(j)) == inf
            dFs(j) = deriv_limit*sign(dFs(j));
        end        
        if abs(dFs(j)) > deriv_limit
            dFs(j) = deriv_limit*sign(dFs(j));
        end
        if abs(dFs(j)) <= min_limit
            if dFs(j) == 0
                dFs(j) = min_limit;
            else
                dFs(j) = min_limit*sign(dFs(j));
            end
        end
    end
end

%%%%% FIND d^2/dt^2 FUNCTION %%%%%
function [ddoty] = derivative_2nd(y_f,delta)
    %add padding to the head and tail
    y_f_forward = [y_f;0;0];
    y_f_back = [0;0;y_f];
    %central difference
    ddoty = (y_f_back - 2*[0;y_f;0] + y_f_forward)/delta^2;
    %trim the padding from the tail and head
    ddoty = ddoty(2:end-1);
    %fix new head(forward difference), new tail not used so we can leave it
    ddoty(1) = (y_f(1) - y_f(2)*2 + y_f(3))/delta^2;
end

%%%%% REAL VALUES OF (ARG)^(1/3) FUNCTION %%%%%
function power_1_by_3 = real_1_by_3(operand)

    %I just wanted things like (-1)^(1/3) to return -1 instead of .5+.866i

    power_1_by_3 = zeros(size(operand));
    for i = 1:length(operand)
        if sign(operand(i)) == -1
            power_1_by_3(i) = -1*(-operand(i))^(1/3);
        else
            power_1_by_3(i) = operand(i)^(1/3);
        end
    end
end

%%%%% MOMENTUM BASIS GENERATOR FUNCTION %%%%%
function vec = get_proposal_vec(length, n)

    %{
    0 : (1,1,1,...,1)/sqrt(length)
    1 : (-1,1,0,...,0)/sqrt(2)
    .
    .
    .
    n : (0,...,0,-1,1)/sqrt(2)
    %}

    if n == 0
        vec = ones(1,length)/sqrt(length);
    else
        vec = zeros(1,length);
        vec(n) = -1/sqrt(2);
        vec(n+1) = 1/sqrt(2);
    end
end
