%With this code, we can look over the data from multiple_run_MHMC.m or 
%single_run_MHMC...

%%%%% INITIALIZE PARAMETERS %%%%%
N = 20;
total_runs = 20;
t_final = .5;
eps = 5;
lambda = 10;
alpha = 0 + 1i;
num_of_calcs = 1*10^6;
%holds data
culm_x_n = zeros(N+1, total_runs);
culm_norm = zeros(total_runs,1);
%if you want to start storing data after a certain point in each run
start_pt = 0;
%how much data to load into matlab at a time
chunk_size = 5*10^6;

%loops over data files
for j = 1:total_runs
    fname1 = ['data/lam',num2str(lambda),'/N',num2str(N),'_t',num2str(t_final),'/N',num2str(N),'_r',num2str(j),'_x_n.out'];
    fname2 = ['data/lam',num2str(lambda),'/N',num2str(N),'_t',num2str(t_final),'/N',num2str(N),'_r',num2str(j),'_Fs.out'];
    fname3 = ['data/lam',num2str(lambda),'/N',num2str(N),'_t',num2str(t_final),'/N',num2str(N),'_r',num2str(j),'_y_n.out'];
    fname4 = ['data/lam',num2str(lambda),'/N',num2str(N),'_t',num2str(t_final),'/N',num2str(N),'_r',num2str(j),'_dFs.out'];
    
    %%%%% LOOP OVER FILE CONTENTS (Fs,dFs,x_n) %%%%%
    % {
    running_x_n = zeros(1,N+1);
    running_norm = 0;
    for i = 1:floor(num_of_calcs/chunk_size)+1
        %%%%% EXTRACTING %%%%%
        %if what is left is smaller than each chunk
        if i == floor(num_of_calcs/chunk_size)+1
            extr_pt = mod(num_of_calcs,chunk_size);
            
            data4 = dlmread(fname4,' ', [(i-1)*chunk_size,0, (i-1)*chunk_size + extr_pt,N]);
            data3 = dlmread(fname3,' ', [(i-1)*chunk_size,0, (i-1)*chunk_size + extr_pt,N]);
            data2 = dlmread(fname2,' ', [(i-1)*chunk_size,0, (i-1)*chunk_size + extr_pt,0]);
        %extract a chunk
        else
            data4 = dlmread(fname4,' ', [(i-1)*chunk_size,0, i*chunk_size,N]);
            data3 = dlmread(fname3,' ', [(i-1)*chunk_size,0, i*chunk_size,N]);
            data2 = dlmread(fname2,' ', [(i-1)*chunk_size,0, i*chunk_size,0]);
        end

        %%%%% HOLDING %%%%%
        %if we don't immediately start holding data
        if start_pt ~= 0
            %if we start holding after this chunk
            if start_pt >= chunk_size
                %update start
                start_pt = start_pt - chunk_size;
            %if there is some data to hold in this chunk
            else
                %hold data EDIT THIS TO BE WHAT YOU WANT
                running_x_n = running_x_n + sum(data3(start_pt:end,:) .* data2(start_pt:end));
                running_norm = running_norm + sum(data2(start_pt:end));
                
                %update start
                start_pt = 0;
            end
        %hold data EDIT THIS TO BE WHAT YOU WANT
        else
            running_x_n = running_x_n + sum(data3 .* data2);
            running_norm = running_norm + sum(data2);
        end
    end
    %}
    %getting y_n
    total_y_n = dlmread(fname1,' ');
    
    culm_x_n(:,j) = total_y_n;
    culm_norm(j) = running_norm;
end
%%%%% DISPLAY DATA PRE-AVERAGING %%%%%
figure();
plot(linspace(0,t_final,N+1),culm_x_n);
legend();
%decide to not consider a run due to some craziness
bad_runs = [];
X_N = [];
NORM_N = [];
for i = 1:total_runs
    if isempty(find(bad_runs == i,1))
        X_N = [X_N,culm_x_n(:,i)];
        NORM_N = [NORM_N,culm_norm(i)];
    end
end

%%%%% DISPLAY AVG DATA %%%%%
lindblad_krylov;
%{
figure();
plot(time,x_t_lind);
%}
hold on;
errorbar(linspace(0,t_final,N+1), sum(transpose(X_N))/length(X_N), std(transpose(X_N)));
title(['Lambda = ', num2str(lambda),', alpha = ', num2str(alpha), ', N = ', num2str(N), ', Calc Steps = ', num2str(numOfCalcs), ', Epsilon = ', num2str(eps), ', Runs= ', num2str(total_runs)]);
hold on;
errorbar(linspace(0,t_final,N+1), sum(transpose(X_N .* NORM_N))/sum(NORM_N), std(transpose(X_N)) .* sqrt(sum((NORM_N/sum(NORM_N)).^2)));
legend('Krylov Time evolution', 'Averaged MC', 'Weighted Average MC');
