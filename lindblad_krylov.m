%
%This script will calculate the values of <'calculation_op'(t)> with Krylov
%time evolution in Hamiltonian:
%   H = hbar*w*(n_operator + 1/2) + lambda * x^4
%w/ Lindblad dynamics of strength 'epsilon' and lindblad operator X.
%
%NOTE: This takes awhile to run!! (with params: 't_final' ~ 2 seconds, 
%20 'steps', 'lambda' = 10)

    %INITIAL PARAMETERS
w = 1;
hbar = 1;
m = 1;
lambda = 10;
alpha_0 = 0 + 1i;
%Regulator value
epsilon = 5;
%Krylov matrix cutoff
cutoff = 100;
t_final = 2.5;
steps = 20;
time = 0:t_final/steps:t_final;

    %INITIAL CONDITION
st_alpha = exp(-(abs(alpha_0).^2)/2)*ones(cutoff+1,1);
for j = 1:cutoff
    st_alpha(j+1:end) = st_alpha(j+1:end) * alpha_0/sqrt(j);
end
st_alpha = st_alpha/sqrt(ctranspose(st_alpha)*st_alpha);
%density matrix
rho = st_alpha*ctranspose(st_alpha);
%reshape into column
rho = sparse(reshape(rho, (cutoff+1)^2, 1));

    %OPERATORS
lind_op = sparse(X_creator(cutoff,w,hbar,m));
H = sparse(H_creator(cutoff,w,hbar,m,lambda));
X = sparse(X_creator(cutoff,w,hbar,m));
N = diag(0:cutoff);
calculation_op = X;
%super op
id = speye(cutoff+1);
liouvillian = 1i* (kron(H, id) - kron(id, H)) + epsilon * (kron(lind_op,ctranspose(lind_op)) - 1/2 * (kron(ctranspose(lind_op)*lind_op,id) + kron(id,ctranspose(lind_op*lind_op))));

    %CALCULATION (GET TIME EVOLUTION)
rho_evolved = expmv_tspan(liouvillian, rho, 0, t_final, steps);

    %CALCULATION (APPLY TIME EVOLUTION)
x_t_lind = zeros(length(time),1);
counter = 0;
for t = time
    counter = counter + 1;
    %reshape our density matrix from a column to a square
    x_t_lind(counter) = trace(reshape(rho_evolved(:,counter),cutoff+1,cutoff+1) * calculation_op);
end

    %PLOT
%figure();
hold on;
plot(time,x_t_lind, '-.');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------------------- Hamiltonian Operator function ------------------
%remember: H = hbar*w*(n_operator + 1/2) + lambda * x^4
function H = H_creator(cutoff, w, hbar, m, lambda)
    p = P_creator(cutoff, w, hbar, m);
    x = X_creator(cutoff, w, hbar, m);
    H = p*p/(2*m) + x*x*m*w*w/2 + x*x*x*x*lambda;
end

%------------------------- position operator function ---------------------
%remember: X = (a^dag + a)*sqrt(hbar/(2*m*w))
function x = X_creator(cutoff, w, hbar, m)
    x = zeros(cutoff+1,cutoff+1);
    for n = 0:cutoff
        if ((n-1) >= 0)
            x((n+1)-1,(n+1)) = sqrt(n);
        end
        if ((n+1) <= cutoff)
            x((n+1)+1,(n+1)) = sqrt(n+1);
        end
    end
    x = x*sqrt(hbar/(2*m*w));%/alpha;
end

%------------------------- momentum operator function ---------------------
%remember: P = i*(a^dag - a)*sqrt(m*w*hbar/2)
function p = P_creator(cutoff, w, hbar, m)
    p = zeros(cutoff+1,cutoff+1);
    for n = 0:cutoff
        if ((n-1) >= 0)
            p((n+1)-1,(n+1)) = -sqrt(n);
        end
        if ((n+1) <= cutoff)
            p((n+1)+1,(n+1)) = sqrt(n+1);
        end
    end
    p = p*1i*sqrt(m*w*hbar/2);%/alpha;
end
